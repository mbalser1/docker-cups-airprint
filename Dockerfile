# base image
ARG ARCH=amd64
FROM $ARCH/ubuntu:focal

# args
ARG TIMEZONE=UTC
ARG VCS_REF
ARG BUILD_DATE

# labels
LABEL maintainer="Michael Balser <michael@balser.cc>" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.name="michaelbalser/docker-cups-airprint" \
  org.label-schema.description="CUPS server with airprint" \
  org.label-schema.version="0.1" \
  org.label-schema.url="https://hub.docker.com/repository/docker/michaelbalser/docker-cups-airprint" \
  org.label-schema.vcs-url="https://gitlab.com/mbalser1/docker-cups-airprint" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.build-date=$BUILD_DATE

ENV DEBIAN_FRONTEND=noninteractive

# Install the packages we need. Avahi will be included
RUN apt-get update \
    && ln -fs /usr/share/zoneinfo/$TIMEZONE /etc/localtime \
    && apt-get install -y --no-install-recommends tzdata \
    && apt-get install -y \
	cups \
	cups-pdf \
  	cups-bsd \
  	cups-filters \
	hplip \
	inotify-tools \
	foomatic-db-compressed-ppds \
	printer-driver-all \
	openprinting-ppds \
	hpijs-ppds \
	hp-ppd \
	python3-cups \
	cups-backend-bjnp \
	ghostscript \
	avahi-utils \
	net-tools \
&& rm -rf /var/lib/apt/lists/*

# This will use port 631
EXPOSE 631/tcp

# We want a mount for these
VOLUME /config
VOLUME /services

# Add scripts
ADD root /
RUN chmod +x /root/*
CMD ["/root/run_cups.sh"]

# Baked-in config file changes
RUN sed -i 's/Listen localhost:631/Listen \*:631/' /etc/cups/cupsd.conf && \
	sed -i 's/Browsing Off/Browsing On/' /etc/cups/cupsd.conf && \
	sed -i 's/<Location \/>/<Location \/>\n  Allow All/' /etc/cups/cupsd.conf && \
	sed -i 's/<Location \/admin>/<Location \/admin>\n  Allow All\n  Require user @SYSTEM/' /etc/cups/cupsd.conf && \
	sed -i 's/<Location \/admin\/conf>/<Location \/admin\/conf>\n  Allow All/' /etc/cups/cupsd.conf && \
	echo "ServerAlias *" >> /etc/cups/cupsd.conf && \
	echo "DefaultEncryption Never" >> /etc/cups/cupsd.conf
