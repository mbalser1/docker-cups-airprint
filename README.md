# <a name="toc"></a> Table of Contents
* [About](#about)
* [Intro](#intro)
* [Multi-arch Image](#multi-arch)
* [Getting Started](#start)
  + [Docker Run](#drun)
  + [Docker Create](#dcreate)
    + [Parameters](#dparams)
  + [Docker Compose](#dcompose)
  + [Docker Build](#dbuild)
* [Using](#using)
* [Notes](#notes)
* [Trouble Shooting](#trouble)
  + [Missing Printer Driver](#missing-driver)
  + [Driver Version](#driver-version)


# <a name="about"></a> [About](#toc)
Fork and modified copy of source code at:
https://github.com/RagingTiger/cups-airprint

# <a name="intro"></a> [Intro](#toc)
This Ubuntu-based Docker image runs a CUPS instance that also works as an AirPrint
relay for printers that are already on the network but not AirPrint capable.
The local Avahi will be utilized for advertising the printers on the network,
or a separate avahi container can be used:

For example: https://hub.docker.com/repository/docker/michaelbalser/docker-avahi


# <a name="multi-arch"></a> [Multi-arch Image](#toc)
The below commands reference a
[Docker Manifest List](https://docs.docker.com/engine/reference/commandline/manifest/)
at [`michaelbalser/docker-cups-airprint`](https://hub.docker.com/repository/docker/michaelbalser/cups-airprint).

Simply running commands using this image will pull
the matching image architecture (e.g. `amd64`, `arm32v7`, or `arm64`) based on
the hosts architecture. Hence, if you are on a **Raspberry Pi** the below
commands will work the same as if you were on a traditional `amd64`
desktop/laptop computer. **Note**: Because the image requires `ubuntu` as its base
image, there is currently no `arm32v6` architecture available. This means if your
target hardware is a **Raspberry Pi Zero** or similar `arm 6` architecture, this
image will not run.

# <a name="start"></a> [Getting Started](#toc)
This section will give an overview of the essential options/arguments to pass
to docker to successfully run containers from the `michaelbalser/docker-cups-airprint` docker
image.

## <a name="drun"></a> [Docker Run](#toc)
To simply do a quick and dirty run of the cups/airprint container:
```
$ docker run \
       -d \
       --name=cups \
       --net=host \
       -v /var/run/dbus:/var/run/dbus \
       --device /dev/bus \
       --device /dev/usb \
       -e CUPSADMIN="admin" \
       -e CUPSPASSWORD="password" \
       michaelbalser/docker-cups-airprint
```
To stop the container simply run:
```
$ docker stop cups
```
To remove the conainer simply run:
```
$ docker rm cups
```
**WARNING**: Be aware that deleting the container (i.e. `cups` in the example)
will permanently delete the data that `docker volume` is storing for you.
If you want to permanently persist this data, see the `docker create` example
[below](#create). Continue reading the *Notes* section for more details about
Docker volumes

+ **Notes**: The `Dockerfile` explicitly sets volumes at `/config` and
`/services`. The necessary configurations done by the `docker container` will be
stored in those directories and will persist even if the container stops. Docker
will store the contents of these directories (located in the container) in
`/var/lib/docker/volumes` (see for reference
[Docker Volumes](https://docs.docker.com/storage/volumes/)).

## <a name="dcreate"></a> [Docker Create](#toc)
Creating a container is often more desirable than directly running it:
```
$ docker create \
       --name=cups \
       --restart=always \
       --net=host \
       -v /var/run/dbus:/var/run/dbus \
       -v /data/cups:/config \
       -v /etc/avahi/services:/services \
       --device /dev/bus \
       --device /dev/usb \
       -e CUPSADMIN="admin" \
       -e CUPSPASSWORD="password" \
       michaelbalser/docker-cups-airprint
```
Follow this with `docker start` and your cups/airprint printer is running:
```
$ docker start cups
```
To stop the container simply run:
```
$ docker stop cups
```
To remove the conainer simply run:
```
$ docker rm cups
```

### <a name="dparams"></a> [Parameters](#toc)
* `--name`: gives the container a name making it easier to work with/on (e.g.
  `cups`)
* `--restart`: restart policy for how to handle restarts (e.g. `always` restart)
* `--net`: network to join (e.g. the `host` network)
* `-v /data/cups:/config`: where the persistent printer configs
   will be stored
* `-v /etc/avahi/services:/services`: where the Avahi service files will
   be generated
* `-e CUPSADMIN`: the CUPS admin user you want created
* `-e CUPSPASSWORD`: the password for the CUPS admin user
* `--device /dev/bus`: device mounted for interacting with USB printers
* `--device /dev/usb`: device mounted for interacting with USB printers

## <a name="using"></a> [Using](#toc)
CUPS will be configurable at http://localhost:631 using the
CUPSADMIN/CUPSPASSWORD when you do something administrative.

If the `/services` volume isn't mapping to `/etc/avahi/services` then you will
have to manually copy the .service files to that path at the command line.

## <a name="notes"></a> [Notes](#toc)
* CUPS doesn't write out `printers.conf` immediately when making changes even
though they're live in CUPS. Therefore it will take a few moments before the
services files update
* Don't stop the container immediately if you intend to have a persistent
configuration for this same reason

## <a name="trouble"></a> [Trouble Shooting](#toc)
Here we are going to discuss the most **common problems** that users have when
trying to setup and configure their printer to work with the
**michaelbalser/docker-cups-airprint** image.

### <a name="missing-driver"></a> [Missing Printer Driver](#toc)
As you might imagine this is **the most common** problem users have when setting
up their printers. While the **michaelbalser/docker-cups-airprint** image possesses
**multiple printer drivers**, it most likely **does not** have every driver for
every printer. This issue can be resolved as follows:

+ Figure out what printer driver you need, open an issue about missing driver,
  necessary package containing said driver will be added to **Dockerfile**.

### <a name="driver-version"></a> [Driver Version](#toc)
Sometimes the right printer driver is installed in the **michaelbalser/docker-cups-airprint**
Docker image, but the **version** is not current. This issue may require one of
two choices to resolve:

+ Download the **docker-cups-airprint** git repo and build a fresh image
  + This will pull the most recent versions of the printer driver from the package
    manager.

+ Download driver **DIRECTLY** from the manufacturer and add it to the image
  + If building a fresh image does not update the version of the driver, then
    you will need the most recent printer driver from the manufacturer.

### <a name="avahi"></a> [Avahi](#toc)
Edit the avahi config file (either on the host or in a avahi container):
```
nano /etc/avahi/avahi-daemon.conf

Change #enable-reflector=no to enable-reflector=yes
```
Afterwards, restart the avahi daemon on host using
```
systemctl restart avahi-daemon.service
```
